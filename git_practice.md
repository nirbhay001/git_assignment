# GIT Practice Command by Nirbhay
Git is a free and open source distributed version control system. It provides facility to track and changes in your source code.

## Important GIt Command and it's uses.
 * Git init : -  This command is use to initialize git repository in the folder .

 * Git add "name_of_file" : - This command use to send the file in staging area .
 * Git commit -m "commit_message" : - Commit changes in staging area to the repository .
 * Git branch : - Lists all branches in the repository and shows the current branch.
 * git checkout "branch-name": -  Switches to the specified branch .
 * git merge "branch-name" : -  Merges the specified branch into the current branch .
 * Git reset : - This command is used to move the current branch to a specified commit .
 * Git Rebase : - This command is used to apply changes from one branch onto another branch . 

   These are some important command in git.

## Git practice command in Exercise.
1. Exercise:  Push a commit you have made.
 * Git start
 * git verify
2. Exercise:  Commit one file.
* git add A.txt
* git commit -m "Commit a file A.txt"
* git verify
3. Exercise:  Commit one file of two currently staged.
* git reset B.txt
* git commit -m "Added a file A.txt"
* git verify
4. Exercise: Ignore unwanted files
* touch .gitignore
 * nano .gitignore and add the file, 
  *.exe
  *.o
  *.jar
 libraries/
 * git add .
 * git commit -m "Add gitignore file"
* git verify
5.  Exercise:  Chase branch that escaped.
* git merge escaped
* git verify
6. Exercise:  merge-conflict.
* git merge another-piece-of-work
* open the terminal editor and solve merge conflict.
* git add equation.txt
* git commit -m "Merge conflict resolved"
* git verify
7. Exercise:  Save-your-work
* git stash
* nano bug.txt // Fixed the bug
* git commit -am "Big fixed"
* git stash pop
* nano bug.txt 
* git add bug.txt
* git commit -a "Finally bug fixed"
* git verify
8. Exercise:  Change branch history.
* git rebase hot-bugfix
* git verify
9. Exercise: Remove ignored file 
* git rm ignored.txt
* git commit -am "Remove the file that should have been ignored"
* git verify
10.  Exercise:  Change a letter case in the filename of an already tracked file.
* git mv File.txt file.txt
* git commit -am "Do Lowercase file_name"
* git verify
11.   Exercise: Fix typographic mistake in the last commit.
* Fix the error in file.
* git add .
* git commit --amend
* git verify
12.  Exercise:  Forge the commit's date.
* Git commit --amend --no-edit --date="1987-01-01"
* git verify
13.    Exercise:   Fix typographic mistake in old commit
* git rebase -i HEAD~2
* It will be open terminal editor , where we edit pick to "edit"
* git add file.txt
* git rebase --continue
* solve the conflict.
* git add file.txt
* git rebase --continue
* git verify
14.   Exercise:   Find a commit that has been lost.
* Git reflog
* git reset --hard HEAD@{1}
* git verify
15.   Exercise:  Split the last commit
* git reset HEAD^
* git add first.txt
* git commit -m "First_file first.txt added"
* git add second.txt
* git commit -m "Second_file second.txt added"
* git verify
16. Exercise: Too many commits.
* git rebase -i HEAD~2
* replace pick with squash
* git verify
17.    Exercise: Make the file executable by default.
* git update-index --chmod=+x script.sh
* git verify
18.   Exercise: Commit part of work 
*  git add -p file.txt
*  split the part
*  choose change regard task 1
*  git commit -m "Commit task-1"
*  choose change regard task 2
*  git commit -m "Commit task-2"
*  git verify
19.   Exercise: Pick your features
* git cherry-pick feature-a
* git cherry-pick feature-b
* git cherry-pick feature-c
* git add -A
* git verify